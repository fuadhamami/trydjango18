from django.conf import settings
from django.core.mail import send_mail
from django.shortcuts import render

from .forms import ContactForm, SingUpForm
from .models import SingUp

# Create your views here.
def home(request):
	title = "Sing Up Now"
	form = SingUpForm(request.POST or None)
	context = {
		"title": title,
		"form": form
	}

	if form.is_valid():
		#form.save()
		#print request.POST['email'] 
		instance = form.save(commit=False)

		full_name = form.cleaned_data.get("full_name")
		if not full_name:
			full_name = "New full name"
		instance.full_name = full_name

		instance.save()
		context = { 
			"title": "Thank you"
		}

	if request.user.is_authenticated() and request.user.is_staff:
		queryset = 	SingUp.objects.all().order_by('-timestamp')
		
		context = {
 		"queryset": queryset
 		}

	return render(request, "home.html", context)
 

def contact(request):
	title = 'Contact Us'
 	form = ContactForm(request.POST or None) 
 	if form.is_valid():
 		# for key, value in form.cleaned_data.iteritems():
 		# 	print key, value
 			#print form.cleaned_data.get(key)
 		form_email = form.cleaned_data.get("email")
 		form_message = form.cleaned_data.get("message")
 		form_full_name = form.cleaned_data.get("full_name")
 		# print email, message, full_name
 		subject = 'Site concact from'
 		from_email = settings.EMAIL_HOST_USER
 		to_email = [from_email, 'toolzark@gmail.com']
 		contact_message = "%s: %s via %s"% (
 			form_full_name,
 			form_message,
 			form_email)
 		some_html_message = """
 		<h1>Hello</h1>
 		"""
 		send_mail(subject,
 			contact_message,
 			from_email,
 			[to_email],
 			fail_silently=False)

 	context = {
 		"form": form,
 		"title": title,
 	}

 	return render(request, "forms.html", context)

def about(request):
	title = 'About Us'

	context = {
		"title": title,
	}

	return render(request, "about.html", context)

def handler404(request):
	page_title = 'Page Not Found'

	context = {
		"page_title": page_title,
	}
	return render(request, "404.html", context)