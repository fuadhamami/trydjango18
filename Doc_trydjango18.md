## istallation

Membuat virtualenv pada folder yang aktif saat ini
`virtualenv .`

Mengaktigkan virtualenv
`source /bin/activate`

install django 1.8

`pip install django==1.8`

cek paket yang telah terinstall
`pip freeze`

```
check installed package again
Django==1.8
argparse==1.2.1
wsgiref==0.1.2
```

membuat project dengan nama trydjango18
`django-admin.py startproject trydjango18`

ops kita memiliki stuktur folder yang sedikit membingungkan, trydjango18 pada virtualenv dan trydjango18 untuk project django. Ini adalah langkah opsional. Untuk memudahkan kita rename dulu folder utama agar terlihat lebih mudah dipahami struktur foldernya.

rename folder trydjango18 menjadi src
`mv trydjango18/ /home/toolzark/trydjango18/src`

Menjalankan server django18
`python manage.py runserver`

## first migration

untuk membuat database atau seluruh setting django menggunakan perintah migrate, setelah kita menjalankan perintah ini django akan membuat default database dengan nama db.sqlite3. Kita bisa merubah setting database di setting.py namaun untuk tutorial ini kita menggunakan sqlite. Jika kita kehilangan file database, kita panggil ulang perintah migrate dan file database akan terbuat lagi.

`python manage.py migrate`

apa yang dilakukan oleh migrate? 
migrate akan melakukan checking kemudian mensinkronasi apps dan database yang belum termigrasi


## admin & superuser

`python manage.py syncdb`

```
You have installed Django's auth system, and don't have any superusers defined.
Would you like to create one now? (yes/no): no
```

jawab "no" karena kita akan membuatnya dengan perintah yang lain yaitu dengan createsuperuser.

> in django 1.9 syncdb will be removed because migrate.

`python manage.py createsuperuser`

```
Username (leave blank to use 'root'): toolzark
Email address: toolzark@gmail.com
Password: l0ck3d
```


## Apps

ini adalah langkah optional karena saya ingin folder ini bisa diakses tanpa root permission

`chmod -R 777 src/*`

membuat app dengan django

`python manage.py startapp newsletter`

ketika kita membuat app kita akan mendapatkan folder baru yang mempunyai nama yang sama dengan aplikasi kita, pada tuturial ini menggunakan aplikasi newsletter, dan kita mendapatkan direktori dan file baru didalam newsletter.

```
newsletter
-- admin.py
-- __init__.py
-- migrations
   -- __init__.py
-- models.py
-- tests.py
-- views.py
```


## first view & url routing

**urls.py** digunakan untuk menjelaskan urls, seperti 127.0.0.1:8000/admin atau 27.0.0.1:8000/blog.
**views.py** digunakan untuk menghandle apa yang pengunjung akan lihat pada urls yang diakses.

sekarang kita mulai untuk membuat homepage, pertama kita membuat fungsi di views.py

```
def home(request):
	return render(request, "home.html", {})
```

request adalah parameter untuk memanggil data dari url yang dikunjungi oleh user, misalkan user masuk ke halaman admin maka request akan akan meminta seluruh data pada url admin dan diberikan ke user, pada terminal ketika request bekerja akan menampilkan pesan

```
"GET /admin HTTP/1.1" 301 0
"GET /admin/ HTTP/1.1" 302 0
"GET /admin/login/?next=/admin/ HTTP/1.1" 200 1915
"GET /static/admin/css/login.css HTTP/1.1" 200 940
"GET /static/admin/css/base.css HTTP/1.1" 200 14049
"GET /static/admin/img/nav-bg.gif HTTP/1.1" 200 265
```
GET adalah aktifitas dari request untuk mendapatkan data

def home(request): home meminta data kepada request, return render(request, "home.html", {})  kemudian request mengembalikannya dengan tempalate home.html. namun kita tidak bisa mendapatkan home.html karena ada 2 syarat yang belum terpenuhi yaitu url dan .html file.
untuk dapat menempilkan .html kita harus mengatur urlnya terlebih dahulu.

untuk url yang sudah ada pada django adalah url admin, mari kita pahami url admin terlebih dahulu yang ada di trydjango18/urls.py

```
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
]
```

**url(r'^admin/'** saat user masuk ke **localhost/admin** url akan memanggil admin project **include(admin.site.urls)** yang berada di **django.contrib**.

pada kasus kita, kita ingin menampilkan homepage dengan request home.html, maka kita buat url kita di trydjango18/urls.py

url(r'^$', 'newsletter.views.home', name='home'),

r'^$' ini adalah regular expression atau biasa disebut regex, pada regex "^" berarti start dan "$" adalah in atau tujuan, misalkan pada admin tujuan kita adalah "admin/", sedangkan "$" menandakan homepage

newsletter.views.home adalah lokasi fungsi dari template kita newsletter adalah nama app, views adalah views.py dan home adalah fungsi home yang berada di dalam views.py.

name='home' adalah 


## django setting overview

all setting of django in setting.py. Lets make some page for newsletter

==urls.py==
```
url(r'^$', 'newsletter.views.home', name='home')
```

==views.py==
```
def home(request):
	return render(request, "home.html", {})
```

setting.py
```
        'DIRS': [os.path.join(BASE_DIR, "templates")],
```

## 8 template configuration

we can config for template or page in setting.py.
find :
```
TEMPLATES = [
    {
	...
	'DIRS': [],
	...
    },
]
```

DIRS that it mean directory, so change directory path to
```
'DIRS': [os.path.join(BASE_DIR, "templates")],
```
that mean directory path in src/templates

## 9 models

create first model call SingUp, edit in ==newsletter/models.py==
```
class SingUp(models.Model):
	email = models.EmailField()
	full_name = models.CharField(max_length=120, blank=True, null=True)
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
	updated = models.DateTimeField(auto_now_add=False, auto_now=True)

	def __unicode__(self): #python 3.3 is __str__
		return self.email
```
then add application definition in ==trydjango18/setting.py==
```
INSTALLED_APPS = (
    'newsletter',
)
```

then initialize the migration with commandline
`python manage.py makemigrations`

```
Migrations for 'newsletter':
  0001_initial.py:
    - Create model SingUp
```
run the migration and put to the database
`python manage.py migrate`

```
Running migrations:
  Rendering model states... DONE
  Applying newsletter.0001_initial... OK
```
## 10 admin
now we add model to admin page

==admin.py==

```
from .models import SingUp

admin.site.register(SingUp)
```

why we can do ".models ", because work inside same python module of newsletter or we used " newsletter.models ". if we want to reference from another apps " from someotherapp.models import SomeOtherModel ".

now displaying for timestamp and last update of account

==admin.py==
```
from .models import SingUp

class SingUpAdmin(admin.ModelAdmin):
	list_display = ["__unicode__", "timestamp", "updated"]
	class Meta:
		model = SingUp

admin.site.register(SingUp, SingUpAdmin)
```

## 11 model form

now we make some different way to show singup form add new file forms.py to newsletter

==forms.py==
```
from django import forms
from .models import SingUp

class SingUpForm(forms.ModelForm):
	class Meta:
		model = SingUp
		fields = ['full_name', 'email', ]
```

email on fields we take from models.py in model SingUp
in another way we can use exclude for change fields

```
	class Meta:
		model = SingUp
		exclude = ['full_name', 'email']
```

but this highly not recomded cause doesn't tell what going on with form it self, not easy and use sparingly. add module and inactive or make comment class Meta: and model = SingUp

==admin.py==
```
from django.contrib import admin
from .forms import SingUpForm
from .models import SingUp

class SingUpAdmin(admin.ModelAdmin):
	list_display = ["__unicode__", "timestamp", "updated"]
	form = SingUpForm
	#class Meta:
	#	model = SingUp


admin.site.register(SingUp, SingUpAdmin)
```

## 12 form validation

now we check vadiation form for example we use email, with python function
def clean_fieldName(). basic validation of email

==forms.py==
```
	def clean_email(self):
		email = self.cleaned_data.get('email')
		return email
```

create validation just for user with edu email address can make registration

==forms.py==
```
	def clean_email(self):
		email = self.cleaned_data.get('email')
		if not "edu" in email:
			raise forms.ValidationError("Please use a valid .EDU email address")
		return email
```

create validation with with usc domain and edu extension and splited by @ and . like normally email address

==forms.py==
```
	def clean_email(self):
		email = self.cleaned_data.get('email')
		email_base, provider = email.split("@")
		domain, extension = provider.split('.')
		if not domain == 'usc':
			raise forms.ValidationError("Please make sure use your USC email.")
		if not extension == "edu":
			raise forms.ValidationError("Please use a valid .EDU email address")
		return email
```

def clean_email(self) mean be checking overiding for clean email data, and " self " being one particular instance of that field

```
if not extension == "edu":
			raise forms.ValidationError("Please use a valid .EDU email address")
```

it's mean user must use domain extension .edu for email address, if not they can't register. we can also do for full name or something else, if we need it

```
	def clean_full_name self.cleaned_data.get('full_name')
		#write validation code(self):
		full_name =
		return full_name
```

## View & Template Context

==views.py==
`return render(request, "home.html", {})`

this mean render gonna conbine with request, with template, with contect "{}". we will show title with user who loggin

==views.py==
```
def home(request):
	title = "My Title %s" %(request.user)
	context = {
		"template_title": title, 
	}
	return render(request, "home.html", context)
```


make if no one user loggin home.html will dispaying welcome, but if any user login page will dispaying title username

==views.py==
```
def home(request):
	title = "Welcome"

	if request.user.is_authenticated():
		title = "My Title %s" %(request.user)

	context = {
		"template_title": title, 
	}
	return render(request, "home.html", context)
```

on home.html if 
`<h1>{{ template_title }}</h1>`
page will show value of variable, in our case is title

`<h1>{{ request.user }}</h1>`
page will show username username

`<h1>{{ user }}</h1>`
user page will show username

we can do this because on setting.py -> TEMPLATES had
```
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
```
those will give us ability to see context variable

## Form in a View

`from .forms import SingUpForm`
that mean we import SingUpForm class on forms.py. show form

==home.html==
```
<form method="" action="">
{{ form }}
</form>
```

==views.py==
```
def home(request):

	form = SingUpForm()
	context = { 
		"form" : form
	}
	return render(request, "home.html", context)
```

add Sing Up buttom

==home.html==
```
<form method="" action="">
{{ form }}

<input type="submit" value="Sing Up"/>

</form>
```

method="POST" : form will sending data
action="" : where gonna go after data sent, if blank it mean after sending data page still over there, but if action have url like /admin then after sending data page will move to admin page

CSRF verification failed : you need verify and protection data to make database not corrupt, to solve this you must add {% csrf_token %}

==home.hmtl==
`<form method="POST" action="">{% csrf_token %}`

==views.py==
form = SingUpForm(request.POST) it's make form is required but if form = SingUpForm(request.POST or None) it's make form can null

if form.is_valid(): that mean make all pass its true in forms.py

instance.save() to make sending data saved to database

==views.py==
```
from django.shortcuts import render
from .forms import SingUpForm

def home(request):
	title = "Welcome"
	form = SingUpForm(request.POST or None)
	context = {
		"title": title,
		"form": form
	}

	if form.is_valid():
		#form.save()
		instance = form.save(commit=False)

		full_name = form.cleaned_data.get("full_name")
		if not full_name:
			full_name = "New full name"
		instance.full_name = full_name

		instance.save()
		context = { 
			"title": "Thank you"
		}
		
	return render(request, "home.html", context)
```

if user doesn't input full name this code will make automatically input with string "New full name". print request.POST['email'] to make email input data will show in django console, but it's not really recomended

## Custom Form in a View

==urls.py==
`url(r'^contact/$', 'newsletter.views.contact', name='contact'),`

==views.py==
```
from .forms import ContactForm, SingUpForm

def contact(request):
 	form = ContactForm(request.POST or None) 
 	if form.is_valid():
 		for key, value in form.cleaned_data.iteritems():
 			print key, value
 			#print form.cleaned_data.get(key)
 		# email = form.cleaned_data.get("email")
 		# message = form.cleaned_data.get("message")
 		# full_name = form.cleaned_data.get("full_name")
 		# print email, message, full_name

 	context = {
 		"form": form,
 	}

 	return render(request, "forms.html", context)
```

==forms.py==
```
from django import forms
from .models import SingUp

class ContactForm(forms.Form):
	full_name = forms.CharField(required=False)
	email = forms.EmailField()
	message = forms.CharField()

class SingUpForm(forms.ModelForm):
	class Meta:
		model = SingUp
		fields = ['full_name', 'email']

	def clean_email(self):
		email = self.cleaned_data.get('email')
		email_base, provider = email.split("@")
		domain, extension = provider.split('.')
		#if not domain == 'usc':
		#	raise forms.ValidationError("Please make sure use your USC email.")
		if not extension == "edu":
			raise forms.ValidationError("Please use a valid .EDU email address")
		return email

	def clean_full_name(self):
		full_name = self.cleaned_data.get('full_name')
		#write validation code.
		return full_name

templates/forms.html

<form method='POST' action=''>{% csrf_token %}
	{{ form.as_p }}

<input type='submit' value='Submit' />
</form>
```

## Setup Email

send_mail(subject, contact_message, from_email, [to_email], fail_silently=False) if we want to save sending email to database make this fail_silently=True. write to views.py to allow grap from setting.py

`from django.conf import settings`

write this code bellow ALLOWED_HOSTS = []

==setting.py ==
```
EMAIL_HOST = 'stmp.gmail.com'
EMAIL_HOST_USER = 'pancensedenk@gmail.com'
EMAIL_HOST_PASSWORD = 'S0d4 g3m8124'
EMAIL_PORT = 587
EMAIL_USE_TLS = False
```

this code didn't working, perhaps because python had some prombren in SMTP server

and test doesn't working
python -m smtpd -n -c DebuggingServer localhost:578

## Static files in django

==settings.py==
```
INSTALLED_APPS = (
    ...
    'django.contrib.staticfiles',
    ...
)


STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(BASE_DIR, "static_in_pro", "static_root")

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static_in_pro", "our_static"),
)
```

`python manage.py collectstatic`
this you have to do when you upload or chanege your static file.

You have requested to collect static files at the destination. location as specified in your settings:

`    /home/toolzark/trydjango18/src/static_in_pro/static_root`

```
This will overwrite existing files!
Are you sure you want to do this? 
```

answer 'yes'
```
63 static files copied to '/home/toolzark/trydjango18/src/static_in_pro/static_root'.
```

if you want to change static file to another foldel, for example the folder is out of src like "/trydjango18/static_in_env/" not in /trydjango18/src/static_root you can change static root

`STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), "static_in_env", "static_root")`

now we move static file to /trydjango18/static_in_env/

```
STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), "static_in_env", "static_root")

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static_in_pro", "our_static"),
)

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), "static_in_env", "media_root")
```

`python manage.py collectstatic`


## 18 Serving Static Files

==urls.py==
```
from django.conf import settings
from django.conf.urls.static import static

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns += static(settings.MEDIA_URL, 
document_root=settings.MEDIA_ROOT)

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), "static_in_env", "media_root")
```

`python manage.py collectstatic`

## add bootstrap

download bootstrap examples navbar-static-top save page as to src/templates/base.html

<br>
<br>

#### Dictionary of runserver message
GET : django has geting data
POST : django has sending data

## Django Templates Include, Inheritance, Blocks

{% verbatin %}
{% endverbatin %}

## Django Templates Include, Inheritance, Blocks


## URL Names as Links

untuk membuat link link pada django template menggunakan format

`{% url 'nama' %}`

pada aplikasi kita sudah mempunyai 3 views home, contact, dan about

```
url(r'^$', 'newsletter.views.home', name='home'),
url(r'^contact/$', 'newsletter.views.contact', name='contact'),
url(r'^about/$', 'trydjango18.views.about', name='about'),
```
```
<li><a href="{% url 'home' %}">Home</a></li>
<li><a href="{% url 'about' %}">About</a></li>
<li><a href="{% url 'contact' %}">Contact</a></li>
```
nama pada url misalkan name='contact' harus sama dengan {% url 'contact' %}, jika tidak akan muncul pesan error, misalkan pada urls.py menggunakan name='c'

`url(r'^contact/$', 'newsletter.views.contact', name='c'),`

dan pada template menggunakan {% url 'contact' %}

`<li><a href="{% url 'contact' %}">Contact</a></li>`

maka muncul pesan error

```
Exception Type: 	NoReverseMatch
Exception Value: 	

Reverse for 'contact' with arguments '()' and keyword arguments '{}' not found. 0 pattern(s) tried: []
```

## Styling MVP Landing Part 2

cara untuk membuat suatu kondisi views pada template adalah

contoh kita ingin menampilkan title pada contact

buat objek title = 'isi title', dan pada content masukkan objek title

```
def contact(request):
	title = 'Contact Us'
    
    context = {
 		"title": title,
 	}
```
kemudian pada template masukkan sintak

```
{% if namaobjek %}
	{{ namaobjek }}
{% endif %}
```

```
		{% if title %}
		<div class="row">
				<h1 class="text-align-center">{{ title }}</h1>
		</div>
		{% endif %}
```


## Django Registration Redux

## Authentication Links in the Navbar

		{% if queryset %}
		<h2>Welcome staff</h2>
		{% for instance in queryset %}
			{% instance %}

		{% endfor %}

		{% endif %}
        
gareng@wayang.edu